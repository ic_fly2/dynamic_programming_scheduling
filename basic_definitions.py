import numpy as np
from scipy import optimize
import csv
import matplotlib.pyplot as plt
import random
from math import floor
import time      
from IPython.core.debugger import Tracer
from collections import Counter, defaultdict
from itertools import count

######### WDS Stuff
# class network():
#     """Defines a WDS with a pump, pipe Tank adn demand"""
class pipe:
    def __init__(self,L,D,Method = "DW",e = 0.0005, nu = 1.307*10**-6 ,rho = 999, C = 120):
        self.L = L
        self.D = D
        self.Method = Method
        self.e = e
        self.C = C
        self.nu = nu
        self.rho =  rho
        
    def headloss(self,q):
        """Returns Headloss in Pa
        q is flowrate in m3/s provide a number:
        (int or float) or np.array of values"""
        if self.Method in {"DW", "Darcy","dw"}:
            return (8*self.f(q)*self.rho*self.L*q**2)/(self.D**5 * np.pi**2)
        
        
        elif self.Method in {"HW", "Hazen", "hw"}:
            raise ValueError("Hazen William not implimented, use DW you ludite!")
        
    def headloss_m(self,q):
        """Returns headloss in meters of water column, see headloss for details"""
        return self.headloss(q)/9806.38
    
    def f(self,q):
        """Uses  Swamee-Jain direct approximation of Colebrook-White"""
        self.q = q
        self.v = q/(0.25*np.pi*self.D**2)
        self.Re = self.v*self.D/self.nu
        return 0.25*(  np.log10(self.e/(self.D * 3.7) + 5.74/self.Re**0.9 ) )**-2
    
    def funcF(self,flow,Delta_h):
            return Delta_h - self.headloss_m(flow)
    
    def flowrate_m(self,Delta_h):
        """Given a pressure differential in m, computes the resulting flowrate"""
        return optimize.newton(func=self.funcF, x0=0.4, args=(Delta_h,))
            
class Hyd_pump(object):
    """Loads a pump curve and models a pump"""
    def __init__(self, model="ETAline-100-100-210-FSD.crv"):
#         self.make = make
        self.model = model
        self.curve_data = self.get_curvedata()
        self.curve = self.get_curve()       
        self.max_flow = self.get_max_flow()
        
    def get_curvedata(self):
        with open(self.model) as f:
            content = f.readlines()

        f.close()

        head = []
        flow =[]
        for el in content:
            try:
                float(el[0])
                c = el.split()
                flow.append(float(c[0]))
                head.append(float(c[1]))

            except:
                pass
        data = ["Flow:", np.array(flow)/1000.0, "Head", np.array(head)]
        return data
    
    def get_curve(self):
        return np.polyfit(self.curve_data[1],self.curve_data[3],2)
    
    def get_max_flow(self):
        return max(np.roots(self.curve))
    
    def get_flow(self,H):
        pass
    
    def get_head(self,q):
        return np.polyval(self.curve,q)
    
        

class Tank(object):
    """
    Defines a Tank in terms of elevation minimum and maximum
    fill and Diameter
    """
    def __init__(self,Elev,Hmin,Hmax,Dia=10, Area=False,levels = 20):
        self.Elev = Elev
        self.Hmin = Hmin
        self.Hmax = Hmax
        self.levels = levels
        if Area:
            self.Area = Area
            self.Diameter = None
        else:
            self.Diameter = Dia
            self.Area = 0.25*np.pi*Dia**2
            
        self.Vol = (Hmax - Hmin)*self.Area
            
def gen_flow_rate(levels,tank,pipe,pump,flow_factor=1):
    """
    Takes a network defined by a pipe, tanks, and a pump
    and returns a dictionary with flow rates converted to
    changes in tank level!
    """
    levels = float(levels)
    
    step = (tank.Hmax - tank.Hmin)/levels
    head = np.arange(tank.Hmin,tank.Hmax+step,step)
    
    def func_flow(q):
        return tank.Elev +pipe.headloss_m(q) - pump.get_head(q)
    
    head = np.arange(tank.Hmin,tank.Hmax+step,step) 
        
    flow = []
    for h in head:
        flowrate = optimize.newton(func_flow, x0=pump.max_flow/2.0)
        flow.append(flowrate*flow_factor)

    flow = np.array(flow)/tank.Area
    return dict(zip(np.round(head,3), flow))       



################ WIND STUFF

def Turb_power(s,Cutin=4,Rated=15,Cutoff=25,TurbName = "V90/3MW",Power=2000,rating=15):
    """Describes the power output of a wind turbine as function of windspeed
    Cutin is the lower wind speed limit, Cutoff is the upper windspeed limit 
    and Limit is the point at which the power no longer increases as the 
    nominal power has been reached.
    
    Example Vestas V90
    Cut In:  4 m/s
    Rated:   15 m/s
    Cut Out: 25 m/s
    
    for partial range of V90/3MW we use to 6th order polynomial fitted to the curve
    """
    if TurbName == "V90/3MW":
        if s < Cutin:
            return 0
        elif s < 11:
            x = np.arange(4,12)
            y = np.array([75, 187, 348, 574, 875, 1257, 1688, 2118])
            p = np.polyfit(x, y, 6)
            return np.polyval(p,s)
        elif s < 17:
            x = np.arange(11,18)
            y = np.array([2118, 2514, 2817, 2958, 2994, 2999, 3000])
            p = np.polyfit(x, y, 6)
            return np.polyval(p,s)
        elif s < Cutoff:
            return 3000
        else:
            return 0
    else:
        factor = Turb_factor(Power,rating)
        if s < Cutin:
            return 0
        elif s < Rated:
            return (s**3)*factor
        elif s < Cutoff:
            return Rated**3*factor
        else:
            return 0

def Turb_factor(Power,rating):
    """
    Given a Rated power and rating speed, computes the conversion
    factor
    """
    return float(Power)/rating**3


def provide_speed_range(x,number_of_steps=7,error='constant',f=None):
    """Turns a vector of wind speeds into an array of wind speeds
    seperated by the windspeed step.
    
    Error = None uses 10% of mean of sample
    
    Ensure wind speed vector is 2 dimensional np array.
    
    f = [.1, 0.6] default None , describes the spread of the error
    """
    
    x = np.array(x,ndmin = 2)
    z = x.size
  
    if f is None:
        if error == 'constant':
            error_size = np.linspace(np.mean(x)*0.1,np.mean(x)*0.1,z)
        elif error == 'good':
            error_size = np.linspace(np.mean(x)*0.1,np.mean(x)*0.16,z)
        elif error == 'bad':
            error_size = np.linspace(np.mean(x)*0.1,np.mean(x)*0.20,z)
        elif error == 'extreme':
            error_size = np.linspace(np.mean(x)*0.15,np.mean(x)*0.3,z)
        else:
            error_size = np.linspace(np.mean(x)*0.1,np.mean(x)*0.1,z)
    else:
        error_size = np.linspace(np.mean(x)*f[0],np.mean(x)*f[1],z)
      
   
    if number_of_steps%2 == 0:
        UL = (number_of_steps-1)/2.0
         #= np.array(,ndmin = 2)
    elif number_of_steps%2 == 1:
        UL = (number_of_steps-1)/2.0
        
    
    wind_range = []
    for i in error_size:
        col = np.linspace(-i,i,number_of_steps)
        wind_range.append(col)
        
        
    np.array(wind_range)
   
    
    return np.transpose(wind_range)+x
    
    
    
def get_wind_data(file_name='WindData.csv',col=4):
    """
    returns wind data froma colum as specified in the 
    options (remember start at 0)
    """
    Speed = []
    with open(file_name, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
    
        for row in spamreader:
            Speed.append(row[col])
        
    return Speed


def make_P_matrix(file_name='ProbabilityMatrix.csv'):
    """
    Returns a Pmatrix as np.array given by the csv
    file specified
    """
    Pmatrix = []
    with open(file_name, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
    
        for row in spamreader:
            Pmatrix.append([float(i) for i in row])
            
    return np.array(Pmatrix), len(Pmatrix)
        

##### length adjustments
def list_length_adjustment(x,L,NP=None):
    """
    Takes an array or list and streches it to length L
    preserving the mean value.
    
    For 2-D items Length is the dimension of the inner 
    most list or array.
    
    Example:
    x = [[1, 2, 3],[4,5,6]]
    print list_length_adjustment(x,6)
    [[1, 1, 2, 2, 3, 3],[4, 4, 5, 5, 6, 6]]
    """
    
    # Check state for first calling
    if NP is None:
        if isinstance(x,list):
            NP = False
        elif isinstance(x,np.ndarray):
            x = x.tolist()
            NP = True

    # check dimension of x
    if type(x[0]) is list:
        #two dimensional list
        if type(x[0][0]) is float:
            print "Two dimensional array detected"
            for  i in range(len(x)):
                print i
                x.append(list_length_adjustment(x.pop(0),L))
                
            if NP:
                return np.array(x)
            else:
                return x
                
              
        else:
            raise ValueError('Only one and 2D lists and arrays accepted')
            
    
    
    if len(x) < L:
        # strech
        if L%len(x) == 0:
            l = int(L/len(x))
            xnew = []
            for item in x:
                for counter in range(0,l):
                    xnew.append(item)
                    
            if NP:
                return np.array(xnew)
            else:
                return xnew
        
        else:
            # find common denominator and call this function again
            xtemp = list_length_adjustment(x,len(x)*L)
            return list_length_adjustment(xtemp,L,NP)
        
    elif len(x) > L:
        # compress
        if len(x)%L == 0:
            l = int(len(x)/L)
            xnew = []
            for j in range(0,L):
                val = 0
                for count in range(0,l):
                    val += x.pop(0)

                xnew.append(val/l)
            
            if NP:
                return np.array(xnew)
            else:
                return xnew
                
        else:
            # find common denominator and call this function again
            xtemp = list_length_adjustment(x,len(x)*L)
            return list_length_adjustment(xtemp,L,NP)
        
    elif len(x) == L:
        # do nothing
        return x
    
        
        
##### Random generation:

def new_state(prob,total=None):
    if not total:
        total = sum(prob)
    
    cd = np.zeros((1,len(prob)))
    for i in range(len(prob)):
        cd[0,i] = sum(prob[0:i+1])
    
    val =  np.random.uniform(0,total)
  
    for i in range(len(prob)):
        if val <= cd[0,i]:
            return i

def gen_state(s,P,T=24):
    """Generates a series of states for 24 time steps"""
    states = []
    for j in range(T):
        states.append(s)
        s = new_state(P[s])
    return states


def gen_state2(t,T=24):
    """Generates a series of states for 24 time steps"""
    states = []
    for j in range(T):

        s = np.random.normal(0,1+j/4)
        states.append(np.random.normal(0,1+j/4))

        return states
    





##### dynamic programming #############################################
def val_det(s):
    """Deterministic version of Val"""
    # variables that change
    global cost, cache, p, defs, level_defs
    
    # fixed definitions
    Energy, D, H_init, T, pump_perf = defs
    levels ,Hmax , Hmin, factor = level_defs
    
    #print s
    if s in cache:
        
        return cache[s]
    else:
        h , i = s 

    if h < Hmin or h > Hmax: # h < Hmin or h > Hmax:
        #count_tot += 1  
        v = 1000000000
    elif i == T:  # i = T
        #count_tot += 1  
        if h >= H_init:
            v = h-H_init
        else:
            v = 1000000000 #*(H_init - h)
    else:
        

        no_pump = val_det((discretise(h-D[i]),i+1))
            
        
        #Energy_cost = Exp([Energy[k,i] for k in range(J_max) ],j)
               
        Energy_cost = Energy[i]
        
        pump = Energy_cost + val_det((discretise(h-D[i]+pumping(h)),i+1))


        v = min(no_pump,pump)
                
        if v == no_pump:
            #costj[i,undiscretise(h),j] = 0
            cost[i,undiscretise(h)] = 0
            #pj[i,undiscretise(h),j] = 0
            p[i,undiscretise(h)] = 0
        elif v == pump:
            #pj[i,undiscretise(h),j] = 1
            p[i,undiscretise(h)] = 1
            #costj[i,undiscretise(h),j] = Energy_cost
            cost[i,undiscretise(h)] = Energy_cost            
        else:
            print "ERROR"

    cache[s] = v
    return v


def print_pump_sched_det(h,t_init = 0,final_cost=[]):
    """Doc string"""
    
    global cost, cache, p, defs
    Energy,D,H_init,T, pump_perf  = defs
    
    Schedule = np.array(range(1,T+1))
    Calc_Cost = [] 
    H_out =[]
    #H_rec = [[] for i in range(J_max)]
    #for k in range(0,J_max): 
    h = H_init
    calc_cost = 0
    sched = []
    #H_rec[k] = []
    for i in range(t_init,T):       

        if p[i,undiscretise(h)] == 1: 
            sched.append(1)
            calc_cost += cost[i,undiscretise(h)]
            h = discretise(h+pumping(h)-D[i])
        elif p[i,undiscretise(h)] == 0:
            sched.append(0)
            h = discretise(h-D[i])
        else:
            raise ValueError("Unexpected state")
            #Tracer()()
            sched.append(np.nan)
            h = discretise(h-D[i])

        #H_out[k].append(h)
            #print pj[i,:,:], i ,undiscretise(h), k 
            #raise NameError('Pj not defined correctly')

        H_out.append(h)
        
    Calc_Cost.append(calc_cost)
    Schedule = np.vstack((Schedule,sched))
            
    
    return Calc_Cost, Schedule, H_out

def call_val_det(H_init,Demand=None, Energy=None ,t_init=0,j_init=0, T=7, tank=None,pump_perf=None):
    """Calls the val solver"""
    
    global p, cost, cache, defs,level_defs
    # define stuff:
#     Price_template  = np.array([[2.0, 10.0,  9.0, 3.0, 7.0, 4.0, 3.0,2.0, 10.0,  9.0, 3.0, 7.0, 4.0, 3.0],
#                                 [2.1, 10.1,  9.1, 3.1, 7.1, 4.1, 3.1,2.0, 10.0,  9.0, 3.0, 7.0, 4.0, 3.0],
#                                 [2.2, 10.2,  9.2, 3.2, 7.2, 4.2, 6.2,2.0, 10.0,  9.0, 3.0, 7.0, 4.0, 3.0]])
  
#     Energy = Price_template # no not fiddle
#    D = [0.1, 0.0, 0.2, 0.1, 0.0, 0.4, 0.1,0.1, 0.0, 0.2, 0.1, 0.0, 0.4, 0.1]

    
    if Energy is None:
        Energy = np.random.rand(1,T) # replaced with function from wind data
    else:
        Energy = np.array(Energy)
       
        
    if Demand is None:
        Demand = np.random.rand(T)/10
    else:
        Demand = np.array(Demand)
        Demand = list_length_adjustment(Demand,T)
        
    
    if pump_perf is None:
        pump_perf = {0.0: 0.0032918188291391898, 0.5: 0.0032918188291391898, 2.0: 0.0032918188291391898, 1.8: 0.0032918188291391898,
                     1.2: 0.0032918188291391898, 0.2: 0.0032918188291391898, 0.4: 0.0032918188291391898, 1.0: 0.0032918188291391898,
                     0.8: 0.0032918188291391898, 0.6: 0.0032918188291391898, 1.6: 0.0032918188291391898, 0.3: 0.0032918188291391898,
                     0.1: 0.0032918188291391898, 0.9: 0.0032918188291391898, 1.7: 0.0032918188291391898, 1.4: 0.0032918188291391898,
                     1.3: 0.0032918188291391898, 0.7: 0.0032918188291391898, 1.9: 0.0032918188291391898, 1.1: 0.0032918188291391898, 1.5: 0.0032918188291391898}


    
    
    defs = [Energy, Demand, H_init,T, pump_perf]
    
    if tank is None:
        tank = Tank(20,0,2,Dia=10)
        #Tank.levels =20
        #Tank.Hmax = 2
        #Tank.Hmin = 0
        #Tank.Area = 78.54
        #factor = float(levels / (Hmax-Hmin))
        #level_defs = [levels ,Hmax , Hmin, factor]
        
    #else:
    factor = float(tank.levels / (tank.Hmax-tank.Hmin))
    level_defs = [tank.levels ,tank.Hmax , tank.Hmin, factor]
    
    
    maxHlevels = int(np.ceil(factor*(tank.Hmax+pumping(tank.Hmin))))+1
    p =     np.zeros((T, maxHlevels )) 
    cost =  np.zeros((T, maxHlevels ))    
    p[:]  =  np.NAN
    cost[:] =np.NAN
    
    cache = {}
    
    final_val = val_det( (H_init,t_init) )
    
    if final_val > 10000:
        #print "no minimum found"
        pass
    
    calc_cost, sched, h_final,  = print_pump_sched_det(H_init,t_init,final_val)
   
    #print final_val, calc_cost, sched, h_final
    return calc_cost, sched, H_init, h_final , final_val,

def compute_stoch_cost(T,Cost,schedule,schedule_DP,P_matrix,Power,j_init,J_max,change_in_H,d_fact,H_rec,tank,pump_perf):
    """
    Computes the operating cost civen a true cost matrix,
    a schedule to follow, the pump conditions and the probability
    matrix.
    
    Computes the stochastic operating cost given
    a certain schedule and the way the wind behaves.
    """
    print "Depreciated stoch cost used"
    
    j = j_init
    J_seq = [j]
    Tot_cost_det = 0.0
    
    fval_det = 0.0
    for i in range(T):
        # detminsitc stuff
        #MATLAB
        #Tot_cost_det += schedule[i][0]*Cost[j,i]
        #fval_det +=schedule[i][0]*Cost[ j_init,i]
        
        #DP
        Tot_cost_det += schedule[1,i]*Cost[j,i]
        #fval_det +=schedule[i]*Cost[ j_init,i]
        j = np.random.randint(J_max)
        
        J_seq.append(j)
        # DP stuff
    
    
    #DP stuff
    Tot_cost_DP = schedule_DP[1+J_seq[0],0]*Cost[J_seq[0],0]
    Tot_cost_DP += schedule_DP[1+J_seq[1],1]*Cost[J_seq[1],1]
    for i in range(2,T):        
        Cost_sim = Cost[:,i:i+24]
        Demand=np.roll(change_in_H*d_fact,-i)
        #print Demand
        Calc_Cost, schedule_DP, H_init, h_final, final_val,  H_rec = \
            call_val(H_rec[J_seq[i]][1], t_init=0, j_init=j, T=T,Demand=Demand,\
            Energy=Cost_sim, J_max=J_max,P_matrix = P_matrix,tank=tank, pump_perf=pump_perf)
        #print schedule_DP[1,1+J_seq[i]]
        Tot_cost_DP += schedule_DP[1+J_seq[i],1]*Cost[J_seq[i],1]
    
    
    return Tot_cost_det, Tot_cost_DP, J_seq, fval_det

def discretise(x):
    global level_defs
    levels ,Hmax , Hmin, factor = level_defs
    # discretise the levels of storage
    return floor(x*factor)/factor

def undiscretise(x):
    global level_defs
    levels ,Hmax , Hmin, factor = level_defs
      
    if int(round(x*factor)) - x*factor > 0.00001:
        print int(x*factor) - x*factor, "rounding error"
    
    return int(round(x*factor))

def Exp(x,j):
    """Returns the expected value of x given the probabilities in row j"""
    global defs
    Energy, D, H_init, T, J_max, P_matrix, pump_perf = defs

    #if P_matrix is None:
    #    P_matrix = np.random.rand(J_max,J_max)
    
    #print np.shape(P_matrix)
    return sum(P_matrix[j,:]*x)

def val(s):
    """Doc string"""
    # variables that change
    global cost, cache, p, defs, level_defs, pj, costj
    
    # fixed definitions
    Energy, D, H_init, T, J_max, P_matrix, pump_perf = defs
    levels ,Hmax , Hmin, factor = level_defs
    
    #print s
    if s in cache:
        
        return cache[s]
    else:
        h , j, i = s 

    if h < Hmin or h > Hmax: # h < Hmin or h > Hmax:
        #count_tot += 1  
        v = 1000000000
    elif i == T:  # i = T
        #count_tot += 1  
        if h >= H_init:
            v = h-H_init
        else:
            v = 1000000000 #*(H_init - h)
    else:
        
        # D in m not m/s3
        # pumping(h) returns m not m/s3
        no_pump = Exp([val((discretise(h-D[i]),k,i+1)) for k in range(J_max) ],j)
            
        
        #Energy_cost = Exp([Energy[k,i] for k in range(J_max) ],j)
        #print np.shape(Energy)       
        Energy_cost = Energy[j,i]
        
        pump = Energy_cost + Exp([val((discretise(h-D[i]+pumping(h)),k,i+1)) for k in range(J_max)],j)


        v = min(no_pump,pump)
                
        if v == no_pump:
            costj[i,undiscretise(h),j] = 0
            cost[i,undiscretise(h)] = 0
            pj[i,undiscretise(h),j] = 0
            p[i,undiscretise(h)] = 0
        elif v == pump:
            pj[i,undiscretise(h),j] = 1
            p[i,undiscretise(h)] = 1
            costj[i,undiscretise(h),j] = Energy_cost
            cost[i,undiscretise(h)] = Energy_cost            
        else:
            print v
            raise ValueError("Argh")

    cache[s] = v
    return v



  
def print_pump_sched(h,j_init,t_init = 0,final_cost=[]):
    """Doc string"""
    
    global cost, cache, p, defs
    Energy,D,H_init,T, J_max, P_matrix, pump_perf  = defs
    
    Schedule = np.array(range(1,T+1))
    Calc_Cost = [] 
    H_out =[]
    H_rec = [[] for i in range(J_max)]
    for k in range(0,J_max): 
        h = H_init
        calc_cost = 0
        sched = []
        H_rec[k] = []
        for i in range(t_init,T):       
            if i == 0:
                j = j_init
            else:
                j = k
            #print i, costj[0,undiscretise(h),j_init]

            if pj[i,undiscretise(h),j] == 1: 
                sched.append(1)
                calc_cost += costj[i,undiscretise(h),j]
                h = discretise(h+pumping(h)-D[i])
            elif pj[i,undiscretise(h),j] == 0:
                sched.append(0)
                h = discretise(h-D[i])
            else:
                #Tracer()()
                sched.append(np.nan)
                h = discretise(h-D[i])
                
            H_rec[k].append(h)
                #print pj[i,:,:], i ,undiscretise(h), k 
                #raise NameError('Pj not defined correctly')
        
        H_out.append(h)
        Calc_Cost.append(calc_cost)
        Schedule = np.vstack((Schedule,sched))
            
    
    return Calc_Cost, Schedule, H_out, H_rec

def pumping(h):
    """Disctionary of pump performance
    
    temporary until data is loaded from saved file and Network"""
    global defs
    pump_perf = defs[-1]
    
    
    return pump_perf[discretise(h)]

def call_val(H_init,Demand=None, Energy=None ,t_init=0,j_init=1, T=7, J_max=7, P_matrix=None, tank=None,pump_perf=None):
    """Calls the val solver"""
    
    global p, cost, cache, defs,level_defs, pj, costj
    # define stuff:
#     Price_template  = np.array([[2.0, 10.0,  9.0, 3.0, 7.0, 4.0, 3.0,2.0, 10.0,  9.0, 3.0, 7.0, 4.0, 3.0],
#                                 [2.1, 10.1,  9.1, 3.1, 7.1, 4.1, 3.1,2.0, 10.0,  9.0, 3.0, 7.0, 4.0, 3.0],
#                                 [2.2, 10.2,  9.2, 3.2, 7.2, 4.2, 6.2,2.0, 10.0,  9.0, 3.0, 7.0, 4.0, 3.0]])
  
#     Energy = Price_template # no not fiddle
#    D = [0.1, 0.0, 0.2, 0.1, 0.0, 0.4, 0.1,0.1, 0.0, 0.2, 0.1, 0.0, 0.4, 0.1]

    
    if Energy is None:
        print "No Energy cost specifed"
        Energy = np.random.rand(J_max,T) # replaced with function from wind data
    else:
        Energy = np.array(Energy)
       
        
    if Demand is None:
        print "No Demand specifed"
        Demand = np.random.rand(T)/10
    else:
        Demand = np.array(Demand)
        Demand = list_length_adjustment(Demand,T)
        
    
    if pump_perf is None:
        print "No pump performance specifed"
        pump_perf = {0.0: 0.0032918188291391898, 0.5: 0.0032918188291391898, 2.0: 0.0032918188291391898, 1.8: 0.0032918188291391898,
                     1.2: 0.0032918188291391898, 0.2: 0.0032918188291391898, 0.4: 0.0032918188291391898, 1.0: 0.0032918188291391898,
                     0.8: 0.0032918188291391898, 0.6: 0.0032918188291391898, 1.6: 0.0032918188291391898, 0.3: 0.0032918188291391898,
                     0.1: 0.0032918188291391898, 0.9: 0.0032918188291391898, 1.7: 0.0032918188291391898, 1.4: 0.0032918188291391898,
                     1.3: 0.0032918188291391898, 0.7: 0.0032918188291391898, 1.9: 0.0032918188291391898, 1.1: 0.0032918188291391898, 1.5: 0.0032918188291391898}


    
    
    defs = [Energy, Demand, H_init,T, J_max, P_matrix, pump_perf]
    
    if tank is None:
        print "No Tank specifed"
        tank = Tank(20,0,2,Dia=10)
        #Tank.levels =20
        #Tank.Hmax = 2
        #Tank.Hmin = 0
        #Tank.Area = 78.54
        #factor = float(levels / (Hmax-Hmin))
        #level_defs = [levels ,Hmax , Hmin, factor]
        
    #else:
    factor = float(tank.levels / (tank.Hmax-tank.Hmin))
    level_defs = [tank.levels ,tank.Hmax , tank.Hmin, factor]
    
    
    maxHlevels = int(np.ceil(factor*(tank.Hmax+pumping(tank.Hmin))))+1
    p =     np.zeros((T, maxHlevels )) 
    cost =  np.zeros((T, maxHlevels ))   
    pj =    np.zeros((T, maxHlevels, J_max ))  
    costj = np.zeros((T, maxHlevels, J_max )) 
    p[:]  =  np.NAN
    cost[:] =np.NAN
    pj[:]  = np.NAN
    costj[:]=np.NAN
    
    cache = {}
    
    final_val = val( (H_init,j_init,t_init) )
    
    if final_val > 10000:
        # print "no minimum found"
        pass
    
    calc_cost, sched, h_final,  H_rec = print_pump_sched(H_init,j_init,t_init,final_val)
   
    #print final_val, calc_cost, sched, h_final
    return calc_cost, sched, H_init, h_final , final_val,  H_rec



def print_schedules_nice(cost,sched,h_final):
    for i in range(len(cost)):
        print '%5s'  % round(cost[i],3) ,
        print sched[i+1], h_final[i]
        

