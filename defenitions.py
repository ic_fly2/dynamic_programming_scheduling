# a range of defenitions needed
from __future__ import division
import datetime
import numpy as np
import time
import matplotlib.pyplot as plt
import random
import time
import scipy
from scipy import optimize, signal
import pandas as pd
from collections import Counter, defaultdict
from itertools import count
from tqdm import *
import copy

large_number = 9999999999999999.9 # float('inf')*0 = nan! so use large number

class DynamicProgram(object):
    """
    Generate a dynamic program to find a set of optimal descissions using the HJB.
    
    define the program by:
    
    Setting intial states via:            set_inital_state(list or int)
    Setting the number of steps via:      set_step_number(int) 
    
    Add a set of descissions:             add_decisions_set(set)
    Add a cost function:                  add_cost_function(function in terms of state )
    
    Add a state change equation:          add_state_eq(function(state))
    Add an expression for the last value: add_final_value_expression(function(state,settings))
    Add limits on the states:             add_state_limits(lower=list or int,upper = list or int)

    See below for examples:
    
    """
    def __init__(self):
        self.settings = {
                        'Lower state limits' : [],
                        'Upper state limits' : [],    
                        'x_set' : set(),
                        }
        self.definitions = {}
        
    def add_state_eq(self,function):
        """Returns a tuple describing the states. 
        Remember to increment the first state, b convention the number of steps.
        Load additional parameters (usually needed for cost and state value with global variables)
        
        Example of a function that changes the state by the decission:
        def new_state(x,s):
            return (s[0]+1,s[1]+x) #Return a tuple, use (s[:-1]+(5,)) to slice tuples.  
                
        """
        self.settings['State eq.'] = function
        
    def add_cost_function(self,function):
        """
        Returns a float or integer describing the cost value.
        Load additional parameters (usually needed for cost and state value with global variables)
        
        Example is a function that simply returns the decision as cost:
        def cost(x,s):
            return x
            
        """
        self.settings['Cost eq.'] = function
    
    def add_final_value_expression(self, function,):
        """
        Returns a float or integer as the final value:
        
        Example is a function that returns the ratio of the initial state and the final state:
        
        def val_T(s,settings):
            return s[1]/float(settings['Initial state'][1])
        """
        self.settings['Final value'] = function
        
        
    def set_step_number(self,step_number):
        """Number of stages / steps. Integer"""
        self.settings['T'] = step_number
        
    def set_inital_state(self,intial_values):
        """Provide the inital state of the states other than the stage number"""
        if type(intial_values) is list:
            self.settings['Initial state'] = intial_values
            #self.settings['Initial state'].insert(0,0)
        elif type(intial_values) is int:
            self.settings['Initial state'] = [intial_values]
            #self.settings['Initial state'].insert(0,0)
        elif type(intial_values) is tuple:
            self.settings['Initial state'] = intial_values
           
        self.settings['Initial state'] = tuple(self.settings['Initial state'])

        
    def add_state_limits(self,lower=[],upper=[]):
        """Add the limits on the state other than the stage number, leave empty if none"""
        if type(lower) is list:
            self.settings['Lower state limits'].extend(lower)
            self.settings['Upper state limits'].extend(upper)
        elif type(lower) is int:
            self.settings['Lower state limits'] = [lower]
            self.settings['Upper state limits'] = [upper]
        
    def add_definition(self,key,value):
        self.definitions[key] = value

    
        
    def solve(self):
        """
        Solves the HJB. Returns the optimal value.
        
        Path and further info is stored in the cache. Access it via 
        retrieve_decisions()
        """
        self.settings['cache'] ={}
        ans =  self._hjb_(self.settings['Initial state'])
        self.retrieve_decisions(return_ans=False)
        return ans
    
    def retrieve_decisions(self,return_ans=True):
        """
        Retrieve the decisions that led to the optimal value
        
        Returns the cost for the different states, the optimal schedule and the states
        that the schedule results in.
        """
        #sched = np.ones(self.settings['T'])*np.nan
        sched = {}
        cost_calc= 0
        states = []

        s = self.settings['Initial state']
        t = 0

        while t < self.settings['T']:
            sched[t] = self.settings['cache'][s][1]
                      
            cost_calc += self.settings['Cost eq.'](sched[t],s,self.definitions)
            states.append(s[1:])

            s = self.settings['State eq.'](sched[t],s,self.definitions)
            t += 1

        states.append(s[1:])

        if return_ans == True:
            return cost_calc, schedule, states
        else:
            self.results ={}
            self.results['cost_calc'] = cost_calc
            self.results['schedule'] = sched
            self.results['states'] =  states
    
    
    def plot_results(self,save=False,name=None):
        plt.figure(figsize=(20,3))
        self.retrieve_decisions(return_ans=False)
        sched_plot = self.results['schedule'].values()
        sched_plot = [[int(i[2]) for i in sched_plot]]
            

        plt.pcolor(sched_plot,vmin=0, vmax=5)
        plt.set_cmap('gray_r')
        cbar = plt.colorbar(ticks=range(6)) #vmin=0, vmax=5
        #cbar.set_label(self.settings['x_set'])
        #cbar = plt.colorbar()
        cbar.set_ticklabels(self.settings['x_set'])
        
        plt.axis([0,self.settings['T'],0,1])
        ax=plt.gca()                            # get the axis
        #ax.set_ylim(ax.get_ylim()[::-1])

        plt.rc('text', usetex=True)
        plt.rc('font', family='serif', size=12)
        plt.xlabel('Time')
        plt.ylabel('')
        plt.axis(([0, self.settings['T'], 0, np.size(np.array(sched_plot),0)]))
        ax.set_aspect('equal', adjustable='box')

        if save == True and name is not None:
            #print pwd+name
            plt.savefig( "D:\Phd\DP\ " + name + ".pdf" ,  format='pdf')
        elif save == True:
            raise ValueError("Please provide a name for figure")

        plt.show()
    
    def return_settings(self):
        return self.settings
    
    def return_cache(self):
        return self.settings['cache']
        
    def add_decisions_set(self,set_of_decisions):
        """
        Add a set of permissable decissions. Must be set of unique integers.
        """
        #if set(set_of_decisions) != set_of_decisions:
        #    raise TypeError('Expected a set unique values, use set() to declare a set')
        self.settings['x_set'] = set_of_decisions
        
    def add_setting(self,key,value):
        self.settings[key] = value
       
    def _hjb_(self,s):
        if self.settings['cache'].has_key(s):
            return self.settings['cache'][s][0]

        # check state bounds
        for c,i in enumerate(s[1:]):
            if i < self.settings['Lower state limits'][c] or i > self.settings['Upper state limits'][c]:
                return large_number
            
        #Check if reached time step limit:
        if s[0] == self.settings['T']:            
            m = self.settings['Final value'](s,self.settings)
            self.settings['cache'][s] = [m, np.nan]
            return m

        # Else enter recursion
        else:
            # state_eq =  self.settings['State eq.']
            # cost = self.settings['Cost eq.']
            state_eq =  self.settings['State eq.']
            cost_eq = self.settings['Cost eq.']
            _hjb_ = self._hjb_
            definitions = self.definitions
            
            p ={}
            for x in self.settings['x_set']:
                p[x] = cost_eq(x,s,definitions)+_hjb_(state_eq(x,s,definitions))

        
            pp = min(p, key=p.get)
            m = p[pp]
            self.settings['cache'][s] = [m, pp]

            return m

class StochasticProgram(DynamicProgram):
    """
    Adds a stochastic component to the dynamic program.
    
    state now is: s where s[0] is the step s[1:-1] is the states of the system and s[-1] is the stochastic state
    
    The transition matrix for the markov chain describing the stochastic bhavior is added by:
    add_transition_matrix(P) with P as a list of lists.
    
    
    """
    def add_transition_matrix(self,P):
        """
        Add the transition matrix as list of lists
        
        eg. P = [[0.4,0.5,0.1],
                 [0.2,0.6,0.2],
                 [0.1,0.5,0.4]]
        
        """
        self.settings['P'] = np.array(P)
        self.settings['Len P'] = len(P)
        
    def retrieve_decisions(self, return_ans=True):
        """
        Retrieve the decisions that led to the optimal value
        
        Returns the cost for the different states, the optimal schedule and the states that the schedule results in.
        """
        schedule = {}
        cost_calc= np.zeros(self.settings['Len P'])
        states = {}
        #Initial decission from intial state             
        
        # determine range of nescessary investigations
        set_of_states = []
        for i in range(self.settings['Len P']): 
            D =  dict((key,value) for key, value in self.settings['cache'].iteritems() if key[2] == i and key[0] == 1)
            if len(D) > 0:
                set_of_states.append(i)
        
        self.set_of_states = set_of_states
        
        for i in set_of_states:
            s = self.settings['Initial state']
            states_part = [s, ]
            schedule_part = []#self.settings['cache'][s][1], ] # first decission 
            
            for t in range(0,self.settings['T']):
                schedule_part.append(self.settings['cache'][s][1])
 
                cost_calc[i] += self.settings['Cost eq.'](schedule_part[t],s,self.definitions)
                s = self.settings['State eq.'](schedule_part[t],(s[:-1]+(i,)),self.definitions)
                states_part.append(s)
                #schedule_part.append(self.settings['cache'][s][1])
                

            states[i] = states_part
            schedule[i] = schedule_part

        if return_ans == True:
            return cost_calc, schedule, states
        else:
            self.results ={}
            self.results['cost_calc'] = cost_calc
            self.results['schedule'] = schedule
            self.results['states'] =  states

    def plot_results(self,save=False,name=None):
        plt.figure(figsize=(20,3))
        self.retrieve_decisions(return_ans=False)
        sched_plot = self.results['schedule'].values()
        sched_plot = [[ int(i[2]) for i in sched_plot[j]] for j in range(len(sched_plot))]
        plt.pcolor(sched_plot)
        plt.set_cmap('gray_r')
        cbar = plt.colorbar(ticks=range(6))
        decisions = self.settings['x_set']
        cbar.set_ticklabels(decisions)
        plt.axis([0,self.settings['T'],0,len(self.results['schedule'].keys())])
        ax=plt.gca()                            # get the axis
        #ax.set_ylim(ax.get_ylim()[::-1])

        plt.rc('text', usetex=True)
        plt.rc('font', family='serif', size=12)
        plt.xlabel('Time')
        plt.ylabel('Error State')
        ax.set_aspect('equal', adjustable='box')

        if save == True and name is not None:
            #print pwd+name
            plt.savefig( "D:\Phd\DP\ " + name + ".pdf" ,  format='pdf')
        elif save == True:
            raise ValueError("Please provide a name for figure")
            
    def print_pump_decisions(self):
        """Make a pretty print out of the pumping decisions"""
        self.retrieve_decisions(return_ans=False)
        print "Wind states considered:", self.set_of_states
        print "Computed cost: ", cost_calc
        #print states
        #print schedule
        
        
        for t in range(self.settings['T']): 
            
            print 'Time step: '+ str(t)
            print '      State: ',
            for i in self.set_of_states:
                print i
                print self.results['states'][i][t],#, states
            print "\r"
            
            print '      Decision: ',
            for i in self.set_of_states:  
                print self.results['schedule'][i][t],#, states
            print "\r"
            
        print 'Final State: ',
        for i in self.set_of_states:        
            print self.results['states'][i][-1],
            
        
    

    def solve(self):
        """
        Solves the HJB. Returns the optimal value.
        
        Path and further info is stored in the cache. Access it via 
        retrieve_decisions()
        """
        self.settings['cache'] = {}
        ans = self._hjb_stoch_(self.settings['Initial state'])
        self.retrieve_decisions(return_ans=False)
        return ans
    
    
    
    
    def _hjb_stoch_(self,s):


        # check state bounds
        for c,i in enumerate(s[1:-1]):
            if i < self.settings['Lower state limits'][c] or i > self.settings['Upper state limits'][c]:
                return large_number# inf*0 is nan not 0 :(
            
        if self.settings['cache'].has_key(s):
            return self.settings['cache'][s][0]
            
        #Check if reached time step limit:
        if s[0] == self.settings['T']:            
            m = self.settings['Final value'](s,self.settings)
            self.settings['cache'][s] = [m, np.nan]
            return m

        # Else enter recursion
        else:
            # Move to local variables for speed
            x_set = self.settings['x_set']
            _hjb_stoch_ = self._hjb_stoch_           
            P = self.settings['P']
            definitions = self.definitions
            state_eq =  self.settings['State eq.']
            cost_eq = self.settings['Cost eq.']

            
            # Loop
            p ={}
            for x in x_set:
                future = sum(_hjb_stoch_(state_eq(x,(s[0:-1]+ (i,)),definitions))*P[s[-1],i] for i in range(len(P)) if P[s[-1],i] > 0 )
                p[x] = (cost_eq(x,s,definitions) + future)

        
            pp = min(p, key=p.get)
            m = p[pp]
            self.settings['cache'][s] = [m, pp]

            return m
     
#### Read in wind data
df_o = pd.read_pickle('Corrected_speeds.pkl')
# df_o  = pd.read_csv('Santa_clara_data.txt',parse_dates={'datetime':[1,0]})
# df_o['datetime'] = df_o['datetime'].map(lambda x: x.replace('24:','00:'))
# a = []
# cnt = 0
# for i in range(len(df_o)-5): #trange
    # cor = df_o['datetime'].ix[i]
    # if cor[0:5] == '00:00':
        # if cnt != 0:          
            # next_day = df_o['datetime'].ix[i+1]
            # cor = next_day.replace('00:10','00:00')
        # else:
            # cnt += 1
            
    
    # a.append(cor)
     
# df_o['datetime'] = pd.Series(a)
# df_o['datetime']=pd.to_datetime(df_o['datetime'], format='%H:%M %m/%d/%Y' )
# df_o = df_o.set_index('datetime')
# df_o = df_o[df_o['Avg Wind Speed @ 140ft [m/s]'] != -99999]
# df_o.index.freq = 'T'


def make_Forecast(wind,order=2,digital_pass=0.1):
    """Low pass filter to generate wind forecast"""
    # Low Pass
    b, a = scipy.signal.butter(order,digital_pass, 'low') #4, 100, 'low'
    #print b, a, len(wind)
    return scipy.signal.filtfilt(b, a,wind )

def digitize_wind(x,J_max,uniform=False,centered=True):
    """Returns the digitalised wind and the errors """
    if uniform:
        x_sorted = np.array(sorted(x))
        bin_edge = np.linspace(0,len(x),num=J_max, endpoint=False, dtype=int)
        bins = x_sorted[bin_edge]
    else:
        if centered:
            bins = np.linspace(-max(abs(x)),max(abs(x))+0.00001,J_max+1) # last bin is empty
        else:
            bins = np.linspace(min(x),max(x),J_max)
    
    #print bins
    bins_states = np.append(bins,max(x))
    
    bin_means = []
    for i in range(J_max):
        bin_means.append((bins_states[i]+bins_states[i+1])/2)

    return np.digitize(x,bins)-1, np.array(bin_means)



class WT(object):
    """Defines a wind turbine"""
    def __init__(self,power,rated,cutin,cutout,name=None,elevation=None):
        self.name = name
        self.Power_rating = power
        self.cutin = cutin
        self.cutout = cutout
        self.rated = rated
        self.power_factor = float(power)/rated**3 
    
    
    def turb_power(self,s):
        """Describes the power output of a wind turbine as function of windspeed
        Cutin is the lower wind speed limit, Cutoff is the upper windspeed limit 
        and Limit is the point at which the power no longer increases as the 
        nominal power has been reached.

        Example Vestas V90
        Cut In:  4 m/s
        Rated:   15 m/s
        Cut Out: 25 m/s
        """
        if s < self.cutin:
            return 0
        elif s < self.rated:
            return (s**3)*self.power_factor
        elif s < self.cutout:
            return self.Power_rating
        else:
            return 0
        
#define the turbines here:
Enercon = WT(800,10.7,3,25,"E53")
Vestas = WT(1800,13,3,20,"V100-1.8")
EWT = WT(500,10,3,25,"EWT") 

def tmatrix(lst,J_max=None):
    """from http://stackoverflow.com/questions/28013878/calculate-transition-matrix-of-letters?lq=1
    defaultdict that'll produce a unique index for each unique character
    encountered in lst
    """
    indices = defaultdict(count().next)
    #print len(set(lst))
    if J_max is None:
        print len(set(lst))
        print set(lst)
        J_max = len(set(lst))
      
    b = np.zeros([J_max,J_max])
    
    Ct = Counter(zip(lst, lst[1:])) # zip together consecutive elements of the list

    for (x, y), c in iter(sorted(Ct.iteritems())): # make sorted iteration to generate sorted trasition matrix
        b[x,y] = float(c)
        #b[indices[x],indices[y]] = float(c)
       
    res = dict((v,k) for k,v in indices.iteritems())
    
    b = np.array(b)
    
    # Normalise 
    for i in range(len(b)):
        div = float(b.sum(axis=1)[i])
        if div > 0:
            b[i] = b[i]/div
        
    return b, indices

def plot_T_mat(T_mat,save=False,name=None):
    plt.figure(figsize=(5,5))
    plt.pcolor(T_mat)
    plt.set_cmap('gray_r')
    plt.colorbar()
    plt.axis([0,len(T_mat),0,len(T_mat)])
    ax=plt.gca()                            # get the axis
    ax.set_ylim(ax.get_ylim()[::-1])
    
    plt.rc('text', usetex=True)
    plt.rc('font', family='serif', size=12)
    plt.xlabel('Error State')
    plt.ylabel('Error State')
    plt.gca().set_aspect('equal', adjustable='box')
    
    if save == True and name is not None:
        #print pwd+name
        plt.savefig( "D:\Phd\DP\ " + name + ".pdf" ,  format='pdf')
    elif save == True:
        raise ValueError("Please provide a name for figure")
    
    plt.show()
	
d = {'x=5':[[420.315068493151, 158.948186528497, 420.262536873156, 392.583224635626],
           [530.497476568133, 156.217616580311, 533.038348082596, 411.489820223445],
           [813.554434030281, 152.720207253886, 812.979351032448, 480.423070379402],
           [1096.61139149243, 147.357512953368, 1096.75516224189, 555.622979640447],
           [1375.91925018025, 141.062176165803, 1382.44837758112, 632.076220722509],
           [1658.97620764239, 132.202072538860, 1660.47197640118, 704.769466341519],
           [1942.03316510454, 121.010362694301, 1946.16519174041, 769.942721034424],
           [2225.09012256669, 105.854922279793, 2226.10619469027, 820.075993875121]],
     
     'x=4':[[386.157173756309, 132.559449736407, 385.398230088496, 308.716610900017],
           [648.594087959625, 129.409235920990, 646.165191740413, 356.343220098679],
           [909.156452775775, 125.808991560513, 908.849557522124, 410.236488402427],
           [1171.53392330383, 120, 1171.53392330383, 465.383088527193],
           [1434.21828908555, 112, 1434.21828908555, 523.036352293994],
           [1696.90265486726, 102, 1696.90265486726, 569.409629671639],
           [1959.58702064897, 90, 1959.58702064897, 603.249588839109],
           [2218.43657817109, 73, 2218.43657817109, 619.542902512335]],
     
     'x=3':[[344.917087238645, 108.707830848248, 345.132743362832, 239.783360744059],
           [575.486661860130, 106.682693395479, 575.221238938053, 272.369988090512],
           [806.056236481615, 103.532479580062, 803.392330383481, 307.463279079000],
           [1034.75126171593, 98.8071588569360, 1035.39823008850, 347.569897351557],
           [1263.44628695025, 92.7317464986311, 1265.48672566372, 385.169851982079],
           [1494.01586157174, 84.4061814150281, 1493.65781710914, 419.009811149549],
           [1724.58543619322, 73.6054483335973, 1723.74631268437, 446.583111211932],
           [1953.28046142754, 60.3295472543385, 1955.75221238938, 466.636420348211]],
     
     'x=2':[[301.802451333814, 87.7814105029753, 299.115044247788, 185.890092440311],
           [498.630136986301, 86.2063035952666, 502.359882005900, 205.943401576589],
           [701.081470800289, 83.2811050523791, 701.769911504425, 228.503374354903],
           [899.783705839943, 79.4558454193723, 903.097345132743, 252.316678954234],
           [1098.48594087960, 74.9555399687761, 1098.67256637168, 273.623319911530],
           [1297.18817591925, 68.2050817928818, 1298.08259587021, 293.676629047808],
           [1497.76496034607, 59.6545014367491, 1493.65781710914, 316.236601826122],
           [1698.34174477289, 48.4037378102586, 1696.90265486726, 332.529915499348]],
     
     'x=1': [[279.307858687816, 71.3552956082992, 279.941002949852, 143.276810525719],
           [448.017303532805, 69.5551734280607, 452.507374631268, 154.556796914876],
           [614.852198990627, 67.0800054302328, 617.404129793510, 172.103442409119],
           [779.812545061284, 63.7047763422857, 784.218289085546, 188.396756082346],
           [950.396539293439, 59.4294861642193, 949.115044247787, 205.943401576589],
           [1117.23143475126, 53.8041043509740, 1117.84660766962, 217.223387965746],
           [1285.94087959625, 46.3786003574903, 1284.66076696165, 227.250042533885],
           [1449.02667627974, 37.8280200013575, 1453.39233038348, 227.250042533885]]
    }

df1 = pd.DataFrame(d['x=1'], columns=['Flow x=1','Head x=1','redundant','Power x=1'])
df1 = df1.drop('redundant',1)
df1['Flow x=1']=df1['Flow x=1']/60/60*1000

df2 = pd.DataFrame(d['x=2'], columns=['Flow x=2','Head x=2','redundant','Power x=2'])
df2 = df2.drop('redundant',1)
df2['Flow x=2']=df2['Flow x=2']/60/60*1000

df3 = pd.DataFrame(d['x=3'], columns=['Flow x=3','Head x=3','redundant','Power x=3'])
df3 = df3.drop('redundant',1)
df3['Flow x=3']=df3['Flow x=3']/60/60*1000

df4 = pd.DataFrame(d['x=4'], columns=['Flow x=4','Head x=4','redundant','Power x=4'])
df4 = df4.drop('redundant',1)
df4['Flow x=4']=df4['Flow x=4']/60/60*1000

df5 = pd.DataFrame(d['x=5'], columns=['Flow x=5','Head x=5','redundant','Power x=5'])
df5 = df5.drop('redundant',1)
df5['Flow x=5']=df5['Flow x=5']/60/60*1000

pump_data = pd.concat([df1,df2,df3,df4,df5],axis=1)



class Hyd_pump(object):
    """Loads a pump curve and models a pump
    data is a data frame decisions is a list of the heading distingtions
    """
    def __init__(self, data,decisions,model="VSD", ):
#         self.make = make
        self.model = model
        self.decisions = decisions
        self.curve_data = data
        self.curve = self.get_char_curve()
        self.validate()
    
    def validate(self):
        """after providng the data, validates the operating range"""
        self.min_flow = {}
        self.max_flow = {}
        for d in self.decisions:
            self.min_flow[d] = self.curve_data['Flow '+d][0]
            self.max_flow[d] = self.curve_data['Flow '+d][7]
        
  

    def get_char_curve(self):
        self.char_curve = {}
        self.char_curve_fit_quality = {}
        
        self.power_curve = {}
        self.power_curve_fit_quality = {}
        
        for d in self.decisions:
            self.char_curve[d] = np.polyfit(self.curve_data["Flow "+d],self.curve_data["Head "+d],2,full=True)
            self.power_curve[d] = np.polyfit(self.curve_data["Flow "+d],self.curve_data["Power "+d],3,full=True)
 

    def get_max_flow(self):
        return max(np.roots(self.curve))
    
    def get_flow(self,decision,H):
        flow = np.roots([self.char_curve[decision][0][0],self.char_curve[decision][0][1],self.char_curve[decision][0][2]-H])
        flow = max(flow)
        if flow >= self.min_flow[decision] and flow <= self.max_flow[decision]:
            return flow
        else:
            return np.nan
        
    def get_head(self,decision,q):
        if q>= self.min_flow[decision] and q <= self.max_flow[decision]:
            return np.polyval(self.char_curve[decision][0],q)
        else:
            return np.nan 
    
    def get_power(self,decision,q):
        return np.polyval(self.power_curve[decision][0],q)
    
    def get_head_and_power(self,decision,q):
        return self.get_head(decision,q), np.polyval(self.power_curve[decision][0],q)

		
class Tank(object):
    """Defines a Tank in terms of elevation and Diameter"""
    def __init__(self,Elev,Hmin,Hmax,Dia=10, Area=False):
        self.Elev = Elev
        self.Hmin = Hmin
        self.Hmax = Hmax
        if Area:
            self.Area = Area
            self.Diameter = None
        else:
            self.Diameter = Dia
            self.Area = 0.25*np.pi*Dia**2
            
        self.Vol = (Hmax - Hmin)*self.Area
        
    def capacity_h(self,out_flow,unit = 'l/s'):
        """
        Calculates how long the tank capacity will last
        
        Inputs
        ------
        out_flow : float, flow rate at which tank is to be emptied
        
        
        Parameters
        ----------
        unit : ['l/s'] or 'm3/h' the unit of the outflow value
        
        Returns
        -------
        time : float, capacity in hours
        """
        
        print unit
        if unit == 'l/s':
            return self.Vol/(out_flow*60.0*60.0/1000.0)
        elif unit =='m3/h':
            return self.Vol/out_flow



# class network():
#     """Defines a WDS with a pump, pipe Tank adn demand"""
class pipe:
    def __init__(self,L,D,Method = "DW",e = 0.0005, nu = 1.307*10**-6 ,rho = 999, C = 120):
        self.L = L
        self.D = D
        self.Method = Method
        self.e = e
        self.C = C
        self.nu = nu
        self.rho =  rho
        
    def headloss(self,q, unit='m3/s'):
        """
        Returns Headloss in Pa
        q is flowrate in m3/s provide a number:
        (int or float) or np.array of values        
        
        Parameters
        ----------
        unit : ['m3/s'] or 'l/s' unit of flow
        
        Returns
        -------
        headloss : float, head loss in Pa
        """
        
        if unit == 'l/s':
            q = q/1000.0
        
        if self.Method in {"DW", "Darcy","dw"}:
            return (8*self.f(q)*self.rho*self.L*q**2)/(self.D**5 * np.pi**2)
        
        
        elif self.Method in {"HW", "Hazen", "hw"}:
            raise ValueError("Hazen William not implimented, use DW you ludite!")
        
    def headloss_m(self,q,unit='m3/s'):
        """Returns headloss in meters of water column, see headloss for details"""
        return self.headloss(q,unit)/9806.38
    
    def f(self,q):
        """Uses  Swamee-Jain direct approximation of Colebrook-White equation"""
        self.q = q
        self.v = q/(0.25*np.pi*self.D**2)
        self.Re = self.v*self.D/self.nu
        return 0.25*(  np.log10(self.e/(self.D * 3.7) + 5.74/self.Re**0.9 ) )**-2
    
    def funcF(self,flow,Delta_h):
            return Delta_h - self.headloss_m(flow)
    
    def flowrate_m(self,Delta_h):
        """Given a pressure differential in m, computes the resulting flowrate"""
        return optimize.newton(func=self.funcF, x0=0.4, args=(Delta_h,))
            

# Electricity tariff
idx = pd.date_range('1/1/2011', periods=25, freq='H')
tariff = pd.Series([6.2000,6.2000,6.2000,6.0000,6.0000,6.0000,6.0000,7.9000,7.9000,7.9000,7.9000,8.5000,
                    8.5000,8.5000,8.5000,19.200,19.200,19.200,19.200,8.5000,8.5000,8.5000,8.5000,6.2000,6.2],
                   index=idx)







def flowrate_gen(x,pump_obj,pipe_obj,tank_obj,number_of_states):
    """Takes the objects and calculates head flow rate to the tank
    x = {1,2,3,4,5,...}
    
    q is a dictionary
        
    Returns 
    -------
    q_pump : Dict of tuples flowrates in l/s and power in kW
    """
    Delta_static = float(tank_obj.Elev+tank_obj.Hmin)
    
    states = np.linspace(tank_obj.Hmin, tank_obj.Hmax, number_of_states)
    
    def hydraulics(q,char_curve,pipe_obj,static):
        """
        Solves the hydraulics given the pump decission
        and the network configuration

        x = {1,2,3,4,5,...}
        
        Returns
        -------
        Difference in headloss in m
                
        """
        return np.polyval(char_curve,q) - pipe_obj.headloss_m(q)-static

    q_pump = {}
    x0 = 0.01
    for s in states:
        try:
            q = scipy.optimize.newton(hydraulics, x0,
                                      args=(pump_obj.char_curve[x][0], 
                                            pipe_obj, 
                                            s+Delta_static))*1000
            
            P = pump_obj.get_power(x,q)
        except RuntimeError:
            # outside of pump performance scope
            q = 0.00001
            P = 1000000
            
            
        x0 = q
        q_pump[s] = (q,P)
       
    return q_pump
	
	
def tank_level_change(q_pump,water_demand,tank_obj):
    """Matrix of time and state, values are new states, for each decision"""
       
    states = np.linspace(tank_obj.Hmin,tank_obj.Hmax,len(q_pump))
    out = {}
    diff = 0
    counter = 0
    for t in range(len(water_demand)):
        for s in states:
            
            flow = q_pump[s][0]-water_demand[t]
            #print flow/tank_obj.Area
            new_state = s+flow/tank_obj.Area
            
            
            #new_state_d = states[np.digitize(new_state,states)]
            
            if new_state >= tank_obj.Hmin and new_state <= tank_obj.Hmax:
                digital_state = states[states <= new_state].max()
                diff +=  new_state - digital_state
                counter += 1
                new_state = digital_state
                
                out[(t,s)] = new_state, q_pump[s][1]  
            else:
                 out[(t,s)] = new_state, large_number
    
    #print 'average error from digitalisation: ', diff/counter
    
    return out
	
def gen_flow_rate(levels,tank,pipe,pump,exact=False):
    """
    Takes a network defined by a pipe, tanks, and a pump
    and returns a dictionary with flow rates
    """
    levels = float(levels)
    
    step = (tank.Hmax - tank.Hmin)/levels
    head = np.arange(tank.Hmin,tank.Hmax+step,step)
    
    def func_flow(q):
        return tank.Elev +pipe.headloss_m(q) - pump.get_head(q)
    
    head = np.arange(tank.Hmin,tank.Hmax+step,step) 
        
    flow = []
    for h in head:
        flow.append(optimize.newton(func_flow, x0=pump.max_flow/2.0))

    flow = np.array(flow)
    if exact:
        return flow
    else:      
        return dict(zip(np.round(head,3), flow))
		
		
def make_pump_dict(tank,pump,pipe,decisions,water_demand,base_demand,Number_of_states):
    q_pump = {}
    q_pump[decisions[0]] = {key: (0,0) for key in np.linspace(tank.Hmin,tank.Hmax,Number_of_states) }

    for d in decisions[1:]:
        q_pump[d] = flowrate_gen(d,pump,pipe,tank,Number_of_states)

    pump_dict = {}
    for d in decisions:
        pump_dict[d] = tank_level_change(q_pump[d],water_demand*base_demand,tank)
        
    return pump_dict
	
	
def cost_from_power(cost,Power,Time_step):
    if Power >= 0:
        return Power*cost*int(Time_step[:-1])
    else:
        return Power*int(Time_step[:-1])*5.5 # Export price
		
		

	
	
def flowrate_gen_real(x,pump_obj,pipe_obj,tank_obj,state):
    """
    EXACTLY like flowrate_gen just for one state, returns a float
    Takes the objects and calculates head flow rate to the tank
    x = {1,2,3,4,5,...}
    
    q is a dictionary
        
    Returns 
    -------
    q_pump : Dict of tuples flowrates in l/s and power in kW
    """
    Delta_static = float(tank_obj.Elev+tank_obj.Hmin)
    
    def hydraulics(q,char_curve,pipe_obj,static):
        """
        Solves the hydraulics given the pump decission
        and the network configuration

        x = {1,2,3,4,5,...}
        
        Returns
        -------
        Difference in headloss in m
                
        """
        return np.polyval(char_curve,q) - pipe_obj.headloss_m(q)-static

    if x == 'x=0':
        return 0, 0
    else:
        x0 = 0.01
        q = scipy.optimize.newton(hydraulics, x0,
                                  args=(pump_obj.char_curve[x][0], 
                                        pipe_obj, 
                                        state+Delta_static))*1000

        P = pump_obj.get_power(x,q)

        return q, P
