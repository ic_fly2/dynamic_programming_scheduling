function res = deterministic_WDS_OP(N,network,energy_cost,demand)
%clearvars;
%close all;
% prob.N = 12;
% prob.approx_number = 7;
% prob.vmax_min = 4.5;
% prob.M  = 999;
% prob.settings.time_limit  = 600;
% prob.Desired_ratio = NaN;
% prob.asymmetry = 'on';
% prob.def =  'van_zyl';
% prob.settings.price = rand(1,48).^2*100;
% prob.settings.DR.status = 'Off';
% prob.settings.DR.desired_ratio = 12;
% prob.settings.DR.rest_period = 30; %minutes
CO2_kWh_av = [404 403 397 392 391 404 418 425 428 430 431 431 432 432 430 430 428 427 425 425 427 426 419 407];
EL_pricing

prob.N =96;
prob.approx_number =7;
% prob.approx_number =7;
prob.settings.OF_spec = 'F1';
% prob.settings.OF_spec = 'F3';
prob.vmax_min = 3;
prob.M  = 999;
prob.settings.time_limit  = 600;
prob.Desired_ratio = NaN;
prob.asymmetry = 'on';
% prob.def = 'van_zyl_norm_171_FSD';
prob.def = 'Lecture_example';
% prob.def = 'Cheddar_mod';
% prob.def = 'Cheddar_other_half';
prob.settings.price = strech(EL.price{2},1); 
prob.settings.DR.status = 'Off';
% prob.settings.DR.status = 'Optimal';
prob.settings.DR.flex = 'Window1';

prob.settings.DR.desired_ratio = NaN;
prob.settings.DR.rest_period = 240; %minutes
prob.settings.DR.reward = 30000;
prob.settings.DR.demand_factor = 1;
prob.settings.solver_gap = 0.00;
prob.settings.solver = 'cplex'; %'intlinprog'; %'cplex' 'gurobi';
%prob.settings.time_analysis = 'Percentage';
%prob.settings.DR.recovery = 240;
%prob.settings.mods.Length_factor = 1;
 
 
prob.settings.mods.status = 'No';
prob.settings.plot = 'Off';
% 
prob.settings.Time_shift =0;
% prob.settings.mod_schedule.status = 'No';
% prob.settings.mod_schedule.sched = [0 0 0];
% prob.settings.mod_schedule.index = 1;

prob.settings.GHG = CO2_kWh_av ;
prob.settings.limit_overfill = 0.1;

prob.settings.Robust.Wind = 'Off'; %{Wind; - }
forecast = rand(1,prob.N)*300;

prob.settings.Robust.Windforecast{1} = forecast*1;
prob.settings.Robust.Windforecast{2} = forecast*1.5;
prob.settings.Robust.Windforecast{3} = forecast*1.2;
prob.settings.Robust.Windforecast{4} = forecast*0.5;
prob.settings.Robust.Windforecast{5} = forecast*0.2;

prob.settings.Robust.status = 'No'; %{Yes,No}
prob.settings.Robust.probability = [0.6 0.1 0.3];% [0 1 0];%[0.1 0.5 0.4];
% prob.settings.Robust.cost{1} = strech(EL.price{2}*0.8,0.2);
% prob.settings.Robust.cost{2} = strech(EL.price{2},1);
% prob.settings.Robust.cost{3} = strech(EL.price{2}*1.2,1.3);

% prob.settings.h0 = [2 2];
% prob.settings.h0 = 0.036895372569841;
 out  = experiment(prob);
% for i = 1:prob.N
%     prob.settings.Time_shift =i-1;
%     out  = experiment(prob);
%     fval(i) = out.fval;
%     GHG(i)  = out.GHG;    
% end



disp(['fval: ' num2str(out.fval)  '  DR: ' num2str(out.DR_D)  '  GHG: ' num2str(out.GHG)])
