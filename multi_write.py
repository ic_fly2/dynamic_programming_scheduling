from __future__ import division
import multiprocessing as mp
import datetime
import numpy as np
import time
import random
import time
import scipy
from scipy import optimize, signal
import pandas as pd
from collections import Counter, defaultdict
from itertools import count
import copy
from defenitions import *
import remaining

large_number = 9999999999999999.9 # float('inf')*0 = nan! so use large number

def cost_d(x,s,definitions):
    """Return a float or integer"""
    # Pump power consumption - Power output of the turbine per time step and error state 
    power_con = definitions['pump_dict'][x][(s)][1]-definitions['Power_table_det'][s[0]]
    if power_con >= 0:
        return power_con*definitions['Tariff'][s[0]]*int(definitions['Time_step'][:-1])/60
    else:
        return power_con*int(definitions['Time_step'][:-1])*5.5/60 # Export price
    

def new_state_d(x,s,definitions):
    """Return a tuple of the state"""
    
    
    return (s[0]+1, definitions['pump_dict'][x][(s)][0])
    

#######
def cost(x,s,definitions):
    """Return a float or integer"""
  
     
    # Pump power consumption - Power output of the turbine per time step and error state 
    power_con = definitions['pump_dict'][x][(s[:-1])][1]-definitions['Power_table'][s[0]][s[2]]
    
    if power_con >= 0:
        return power_con*definitions['Tariff'][s[0]]*int(definitions['Time_step'][:-1])/60
    else:
        return power_con*int(definitions['Time_step'][:-1])*5.5/60 # Export price
    

def new_state(x,s,definitions):
    """Return a tuple o fthe state"""
    return (s[0]+1, definitions['pump_dict'][x][(s[:-1])][0], s[2])
    

def val_T(s,settings):
    """Return a float or int"""
    if s[1] < settings['Initial state'][1]:
        return large_number
    else:
        return 0
    
def val_T2(s,settings):
    """Return a float or int"""
    return s[1]*314*8

############################
def cost_from_schedule(Initial_fill,schedule,pump_obj,pipe_obj,tank_obj,turbine_power,base_demand,water_demand,Time_step,Tariff):
    """
    Computes the actual energy consumption and operating cost of a schedule
    
    Returns
    -------
    cost
    """
    Cost = 0
    state = Initial_fill
    for t, P_t in enumerate(turbine_power):
        q, P_p = flowrate_gen_real(schedule[t],pump_obj,pipe_obj,tank_obj,state)
        
        state = state+(q - base_demand*water_demand[t] )/tank_obj.Area
        
        Power_balance = P_p - P_t
        Cost += cost_from_power(Tariff[t],Power_balance,Time_step) 
        
    return Cost , state# in p

def worker(day,J,Time_step,q):
    try:
        #Time_step = ['15T','30T','60T'][Time_step_n]
       
        #Time_step = '120T'
        Filter = 0.1
        base_demand = 150.0

        tank = Tank(15,0,5,Dia=20)
        # print 'Tank capacity in hours:', tank.capacity_h(base_demand)
        Number_of_states = 400
        initial_fill_level = np.linspace(tank.Hmin,tank.Hmax,Number_of_states)[0]#[int(Number_of_states/2)]

        pipe1 = pipe(15000,0.53) #selected pipe!

        #plot_T_mat(T_mat,save=False,name='T_mat_J='+str(J)+'Time_step='+Time_step)
        decisions = ['x=0','x=1','x=2','x=3','x=4','x=5']           
        pump = Hyd_pump(pump_data,['x=1','x=2','x=3','x=4','x=5'])

        turb = Enercon # Vestas # Enercon # EWT
        df_o['Power']= df_o['Avg Wind Speed @ 140ft [m/s]'].map(lambda x: turb.turb_power(x))
        df_o['Forecasted Power'] = make_Forecast(df_o['Power'],order=1,digital_pass=Filter)
        df = df_o.resample(Time_step).mean()
        df = df.dropna()        
        
        No_of_time_steps = int(24*60/float(Time_step[:-1]))

        df['Forecasted error Power'] = df['Power']-df['Forecasted Power'] 
        #df['Forecast error'] = df['Avg Wind Speed @ 140ft [m/s]']-df['Forecasted wind'] 
        df['Forecast error state'], states = digitize_wind(df['Forecasted error Power'],J)
        T_mat, indices = tmatrix(df['Forecast error state'],J_max=J)

        # for j in range(J):
            # df['Forecasted wind at J='+str(j)] = df['Forecasted wind']+states[j]-states[df['Forecast error state']] 
            # df['Power at J='+str(j)] = df['Forecasted wind at J='+str(j)].map(lambda x: turb.turb_power(x))

        # df['Forecasted Power'] = df['Forecasted wind'].map(lambda x: turb.turb_power(x))
        # df['True Power'] = df['Avg Wind Speed @ 140ft [m/s]'].map(lambda x: turb.turb_power(x))
        df_F = pd.DataFrame()
        for j in range(J):
            df_F['Forecasted power at J='+str(j)] = df['Forecasted Power']+states[j]-states[df['Forecast error state']] 
    
        df_F[df_F < 0 ] = 0
        df_F[df_F > 800 ] = float(turb.Power_rating)
        
        #T_mat, indices = tmatrix(df['Forecast error state'],J_max=J)

        Tariff = tariff.resample(Time_step ).ffill().tolist()[:-1]
        Tariff_o = tariff.resample(Time_step ).ffill().tolist()[:-1]

        #water_demand
        idx = pd.date_range('1/1/2011', periods=49, freq='30T')
        water_demand = pd.Series([0.45283019, 0.45283019, 0.50943396, 0.50943396, 0.45283019, 0.45283019,
                                  0.50943396, 0.50943396, 0.67924528, 0.67924528,  1.24528302,  1.24528302,
                                  1.35849057, 1.35849057, 1.13207547, 1.13207547,  0.90566038,  0.90566038,
                                  0.79245283, 0.79245283, 0.8490566,  0.8490566 ,  0.90566038,  0.90566038,
                                  0.90566038, 0.90566038, 1.0754717,  1.0754717 ,  1.24528302,  1.24528302,
                                  1.35849057, 1.35849057, 1.81132075, 1.81132075,  2.03773585,  2.03773585,
                                  1.69811321, 1.69811321, 1.47169811, 1.47169811,  0.90566038,  0.90566038,
                                  0.56603774, 0.56603774, 0.56603774, 0.56603774,  0.56603774,  0.56603774, 0.45283019],
                                 index=idx)
        water_demand = water_demand.resample(Time_step).ffill().tolist()
        water_demand = water_demand[0:No_of_time_steps]
        water_demand = np.array(water_demand) / np.array(water_demand).mean()


        #results =pd.read_pickle('temp_results')
        # Stoch defenitions
        pumping_stoch = StochasticProgram()
        pumping_stoch.add_transition_matrix(T_mat)
        pumping_stoch.set_step_number(No_of_time_steps)
        pumping_stoch.add_decisions_set(set(decisions))
        pumping_stoch.add_cost_function(cost)
        pumping_stoch.add_state_eq(new_state)
        pumping_stoch.add_final_value_expression(val_T2)
        pumping_stoch.add_state_limits(lower=[tank.Hmin,0],upper = [tank.Hmax,J])



        pumping_stoch.add_definition('Time_step',Time_step)


        # Deterministic
        pumping = DynamicProgram()
        pumping.set_step_number(No_of_time_steps)
        pumping.add_decisions_set(set(decisions))
        pumping.add_cost_function(cost_d)
        pumping.add_state_eq(new_state_d)
        pumping.add_final_value_expression(val_T2)
        pumping.add_state_limits(lower=tank.Hmin,upper = tank.Hmax)
        pumping.set_inital_state( (0,initial_fill_level) )

        pumping.add_definition('Time_step',Time_step)

        pumping_ideal = copy.deepcopy(pumping)


        ## run through the day
        dec = []
        dec_ideal = []
        dec_forecast = []
        cost_stoch = [] # acutal cost of operation
        cost_stoch_calc = [] # estimated cost of ops
        cost_forecast = [] # acutal cost of operation
        cost_forecast_calc = [] # estimated cost of ops
        cost_ideal = [] # acutal cost of operation
        cost_ideal_calc = [] # estimated cost of ops

        state_sim_stoch = np.zeros(No_of_time_steps+1)
        state_sim_stoch[0] = initial_fill_level
        state_sim_ideal = np.zeros(No_of_time_steps+1)
        state_sim_ideal[0] = initial_fill_level
        state_sim_forecast = np.zeros(No_of_time_steps+1)
        state_sim_forecast[0] = initial_fill_level

        fill_level_stoch = initial_fill_level
        fill_level_ideal = initial_fill_level
        fill_level_forecast = initial_fill_level
        fill_level_stoch_record = []
        fill_level_ideal_record = []
        fill_level_forecast_record = []

        P_stoch = np.zeros(No_of_time_steps)
        P_ideal = np.zeros(No_of_time_steps)
        P_forecast = np.zeros(No_of_time_steps)

        init = int(No_of_time_steps*day)

        for t in range(No_of_time_steps):
            init += t

            fill_level_stoch_record.append(fill_level_stoch)
            fill_level_ideal_record.append(fill_level_ideal)
            fill_level_forecast_record.append(fill_level_forecast)

            # Define wind states
            df_slice = df.ix[init:init+No_of_time_steps,:]
            Power_table = df_F.ix[init:init+No_of_time_steps,:] #df_slice.ix[:,5:int(J*2)+5:2] 
            Power_table = Power_table.as_matrix() # Array of the set of power from the wind turbine
            Power_table_det = df_slice['Forecasted Power']
            Power_table_true = df_slice['Power']
            current_power =  df_slice['Power'][0]
            
            # set current state to correct power
            Power_table[0][:] = current_power
            Power_table_det[0] = current_power
            # cheat to verfy
            #Power_table = np.array([df_slice['True Power'].as_matrix().tolist()]*J).T


            wind_error = int(df_slice['Forecast error state'][0]) # Inital J value


            #update for new solution
            pump_dict = make_pump_dict(tank,pump,pipe1,decisions,water_demand,base_demand,Number_of_states)


            pumping.add_definition('Tariff',Tariff)    
            pumping.add_definition('Power_table_det',Power_table_det)
            pumping.add_definition('pump_dict',pump_dict)  
            pumping.settings['Initial state'] = (0,fill_level_forecast)   


            pumping_ideal.add_definition('Tariff',Tariff)
            pumping_ideal.add_definition('Power_table_det',Power_table_true)
            pumping_ideal.add_definition('pump_dict',pump_dict)
            pumping_ideal.settings['Initial state'] = (0,fill_level_ideal)

            pumping_stoch.add_definition('Tariff',Tariff)   
            pumping_stoch.add_definition('Power_table',Power_table)
            pumping_stoch.add_definition('pump_dict',pump_dict)
            pumping_stoch.settings['Initial state'] = (0,fill_level_stoch,wind_error)

            cost_stoch_calc.append(pumping_stoch.solve()) # Solve to DP
            cost_ideal_calc.append(pumping_ideal.solve())
            cost_forecast_calc.append(pumping.solve())

            # Record the solutions and actual analysis
            # Stoch
            d_stoch = pumping_stoch.results['schedule'][wind_error][0]

            q_stoch, P_stoch[t] = flowrate_gen_real(d_stoch,pump,pipe1,tank,state_sim_stoch[t])
            state_sim_stoch[t+1] = state_sim_stoch[t]+(q_stoch - base_demand*water_demand[t] )/tank.Area
            cost_stoch.append(cost_from_power(Tariff[t],P_stoch[t],Time_step))

            dec.append(d_stoch)

            # ideal
            d_ideal = pumping_ideal.results['schedule'][0]
            q_ideal, P_ideal[t] = flowrate_gen_real(d_ideal,pump,pipe1,tank,state_sim_ideal[t])
            cost_ideal.append(cost_from_power(Tariff[t],P_ideal[t],Time_step))
            state_sim_ideal[t+1] = state_sim_ideal[t]+(q_ideal - base_demand*water_demand[t] )/tank.Area
            dec_ideal.append(d_ideal)

            # forecast
            d_forecast = pumping.results['schedule'][0]
            q_forecast, P_forecast[t] = flowrate_gen_real(d_forecast,pump,pipe1,tank,state_sim_forecast[t])
            cost_forecast.append(cost_from_power(Tariff[t],P_forecast[t],Time_step))
            state_sim_forecast[t+1] = state_sim_forecast[t]+(q_forecast - base_demand*water_demand[t] )/tank.Area
            dec_forecast.append(d_forecast)



            #update after call:
            fill_level_stoch = pump_dict[d_stoch][(0,fill_level_stoch)][0]
            fill_level_ideal = pump_dict[d_ideal][(0,fill_level_ideal)][0]
            fill_level_forecast = pump_dict[d_forecast][(0,fill_level_forecast)][0]

            # update water demand
            water_demand = np.roll(water_demand,-1)
            Tariff = np.roll(Tariff,-1)


        fill_level_stoch_record.append(fill_level_stoch)
        fill_level_ideal_record.append(fill_level_ideal)
        fill_level_forecast_record.append(fill_level_forecast)


        cost_ideal_sim, final_state_ideal = cost_from_schedule(initial_fill_level,
                                            dec_ideal,
                                            pump,pipe1,tank,df_slice['Power'], base_demand,water_demand,Time_step,Tariff_o) 

        cost_det_sim, final_state_det = cost_from_schedule(initial_fill_level,
                                            dec_forecast,
                                            pump,pipe1,tank,df_slice['Power'],base_demand,water_demand,Time_step,Tariff_o)

        cost_stoch_sim, final_state_stoch = cost_from_schedule(initial_fill_level,
                                            dec,
                                            pump,pipe1,tank,df_slice['Power'],base_demand,water_demand,Time_step,Tariff_o)




        res = ['waste1',day, J, Time_step, turb.name, base_demand, 1.2 , Filter,
                   dec,dec_ideal, dec_forecast,
                   cost_stoch_sim,cost_ideal_sim,cost_det_sim,
                   initial_fill_level,final_state_stoch, final_state_ideal, final_state_det,'waste2' ]   
                   
    except:
        res = ['waste1',day, J, Time_step, turb.name, base_demand, 1.2 , Filter, np.nan ]

    q.put(res)
    return res

fn = 'M:/results/office_pc.txt'

# def worker(arg1,arg2, q):
    # '''stupidly simulates long running process'''
    # start = time.clock()
    # s = 'this is a test'
    # txt = s
    # for i in xrange(200000):
        # txt += s 
    # done = time.clock() - start
    # with open(fn, 'rb') as f:
        # size = len(f.read())
    # res = 'Process' + str(arg1), arg2, str(size), done, 'la li lu', 5623 # line that gets written
	
    # q.put(res)
    # return res
	
	
    

def listener(q):
    '''listens for messages on the q, writes to file. '''

    f = open(fn, 'wb') 
    while 1:
        m = q.get()
        if m == 'kill':
            f.write('killed')
            break
        f.write(str(m) + '\n')
        f.flush()
    f.close()

def main():
    #must use Manager queue here, or will not work
    manager = mp.Manager()
    q = manager.Queue()    
    pool = mp.Pool(mp.cpu_count() -2)

    #put listener to work first
    watcher = pool.apply_async(listener, (q,))

    #fire off workers
    jobs = []
    for i in [(375, 5, '60T'), (375, 11, '30T'), (375, 11, '15T'), (375, 5, '15T')]: #remaining.remain[0:3]):
    #    jobs.append(pool.apply_async(worker, (i[0],i[1],i[2], q)))
        jobs.append(pool.apply_async(worker, (i[0],i[1],i[2], q)))

    # collect results from the workers through the pool result queue
    for job in tqdm(jobs): 
        job.get()

    #now we are done, kill the listener
    q.put('kill')
    pool.close()

print "starting main loop" 
    
if __name__ == "__main__":
   main()