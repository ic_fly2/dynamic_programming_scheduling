function res = triarea(b,h)
res.a = 0.5*(b.* h);
res.b = b;
res.h = h;
res.G = [0.0419 0.0333 NaN 0.3598];
res.prob.N =6;
res.prob.approx_number =7;
% prob.approx_number =7;
res.prob.settings.OF_spec = 'F1';
% prob.settings.OF_spec = 'F3';